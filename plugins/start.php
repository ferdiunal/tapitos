<?php
/**
 * Created by PhpStorm.
 * User: ferdiunal
 * Date: 02.04.2016
 * Time: 12:01
 */
use Plugins\Root\Sidebar;

if( ! function_exists('root_sidebar')){

    function root_sidebar(){
        $root_sidebar = new Sidebar();
        return $root_sidebar->run();
    }

}