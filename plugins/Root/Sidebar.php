<?php
/**
 * Created by PhpStorm.
 * User: ferdiunal
 * Date: 01.04.2016
 * Time: 22:43
 */

namespace Plugins\Root;

class Sidebar
{

    private $data = null;


    private function make()
    {

        $result = '<ul class="sidebar-nav">';
        if(isset(route2json('back')['single'])){
            foreach (route2json('back')['single'] as $route) {
                $active = $route['isActive'] ? ' class="active current"' : null;
                $result .= "<li{$active}><a href='{$route['url']}' title='{$route['title']}'><span class=\"sidebar-text\">{$route['title']}</span></a></li>";
            }
        }
        if(isset(route2json('back')['group'])){
            foreach (route2json('back')['group'] as $route) {
                $in = $route["current"] ? ' in' : null;
                $childs = "<ul class='submenu collapse{$in}'>";
                foreach ($route['child'] as $child) {
                    $active = $child['isActive'] ? ' class="active current hasSub"' : null;
                    $childs .= "<li{$active}><a href='{$child['url']}' title='{$child['title']}'>{$child['title']} </a></li>";
                }
                $childs .= '</ul>';
                $current = $route['current'] ? ' class="active current hasSub"' : null;
                $result .= "<li{$current}><a href='#' title='{$route['title']}'>{$route['title']} <span class=\"fa arrow\"></span></a>{$childs}</li>";
            }
        }

        $result .= '</ul>';
        $this->data = $result;
        return $this;
    }

    public function run()
    {
        return $this->make()->data;
    }

}