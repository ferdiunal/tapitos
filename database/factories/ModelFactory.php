<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    $first_name = $faker->firstNameMale;
    $last_name = $faker->lastName;
    return [
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(100),
        'meta'  =>  json_encode([
            'display_name'  =>  $first_name . ' ' . $last_name,
            'first_name'    =>  $first_name,
            'last_name'     =>  $last_name,
            'tc_no_check'         =>  true,
            'sex'  =>  'male',
            'cell_phone'    =>  $faker->phoneNumber,
            'activated_code'    =>  str_random(64),
            'reset_password_code'   =>  str_random(64),
        ])
    ];
});
