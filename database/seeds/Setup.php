<?php

use Cartalyst\Sentinel\Hashing\Sha256Hasher;
use Illuminate\Database\Seeder;
use \App\Models\User;
class Setup extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        return app('oAuth')->seeder();
    }
}
