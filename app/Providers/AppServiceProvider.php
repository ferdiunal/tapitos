<?php

namespace App\Providers;

use App\Models\Activation;
use Illuminate\Support\ServiceProvider;
use Plugins\PluginProviders;
use ReCaptcha\ReCaptcha;
use Validator;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $secret = $this->app->config->get('recaptcha.secret_key');
        $recaptcha = new ReCaptcha($secret);
        Validator::extend('reCaptcha', function($attribute, $value, $parameters, $validator)use($recaptcha) {
            return $recaptcha->verify($value,request()->ip()) ?? false;
        });

        Validator::extend('alpha_spaces', function($attribute, $value)
        {
            return preg_match('/^[\pL\s]+$/u', $value);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        \View::addNamespace('Front',resource_path('views/front_end'));
        \View::addNamespace('Back',resource_path('views/back_end'));
        $this->app->register(AuthRouteServiceProvider::class);
        $this->app->register(BackRouteServiceProvider::class);
    }
}
