<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\Auth\Login as Request;
use App\Http\Requests\Auth\PassCode;
use App\Http\Controllers\Controller;
class Login extends Controller
{

    /**
     * @return mixed
     */
    public function logout()
    {
        \Sentinel::logout();
        return redirect()->route('auth@login');
    }

    /**
     * @return mixed
     */
    public function index()
    {
        return $this->view('Front::auth.login')->nest('loginForm','Front::assets.loginForm',['title' => 'Giriş Yap','dataAlert' => 'right']);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        $remember = $request->has('remember');
        $credentials = $request->except(['remember']);
        return app('oAuth')->login($credentials,$remember);

    }

    /**
     * @param PassCode $passCode
     * @return mixed
     */
    public function pass_code(PassCode $passCode)
    {
        $remember = $passCode->has('remember');
        $credentials = $passCode->except(['remember']);
        return app('oAuth')->pass_code($credentials,$remember);
    }
}
