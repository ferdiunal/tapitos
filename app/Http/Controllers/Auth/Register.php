<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\Register as Request;

class Register extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->view('Front::auth.register')->nest('loginForm','Front::assets.loginForm',['title' => 'Zaten Üyeyim','dataAlert' => 'left']);
    }

    public function store(Request $request)
    {
        return app('oAuth')->register($request->except(['email_confirmation']));
    }

}
