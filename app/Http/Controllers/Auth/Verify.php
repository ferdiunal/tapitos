<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class Verify extends Controller
{
    public function activated($id,$code)
    {

        if(!\Sentinel::guest() && \Sentinel::getUser()->activations()->completed === true ||
            !\Sentinel::guest() && \Sentinel::getUser()->activations()->completed === false &&
            \Sentinel::getUser()->id !== $id){
            return redirect()->route('home');
        }

        $user = \Sentinel::findById($id);

        if(!$user){
            abort(404,'Böyle bir kullanıcı bulunamadı !');
        }

        if( !\Activation::exists($user,$code)){
            return 'Test';
            return redirect()->route('home')->withErrors('Hesabınız daha önce etkinleştirilmiş.');
        }

        \Activation::complete($user,$code);
        \Sentinel::loginAndRemember($user);

        return redirect()->route('home')->withSuccess('Hesabınız başarılı bir şekilde etkinleştirildi.');
    }

    public function reminder($code)
    {
        return $code;
    }
}
