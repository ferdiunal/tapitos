<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\Auth\Reminder as Request;
use App\Http\Controllers\Controller;

class Reminder extends Controller
{
    public function index()
    {
        return $this->view('Front::auth.reminder')->nest('loginForm','Front::assets.reminderForm',['title' => 'Yeni Şifre Talebi','dataAlert' => 'right']);
    }

    public function store(Request $request)
    {
        $email = $request->except(['g-recaptcha-response']);
        $user = \Sentinel::findByCredentials(['email' => $email]);
        if(!isset($user)){
            return response([ 'global_error'    =>  'Girmiş olduğunuz eposta adresine ait bir üyelik bulunamadı.' ],423);
        }
        $reminder = \Reminder::create($user);
        $reminder_code = $reminder->code;
        $mailData = [
            'email'  => $email,
            'display_name'  =>  $user->first_name . ' ' . $user->last_name
        ];

        /*
         * TODO mail gönderim işlemi yapılacak
         */

    }

}
