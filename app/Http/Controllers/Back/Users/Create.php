<?php

namespace App\Http\Controllers\Back\Users;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class Create extends Controller
{
    public function index()
    {
//        return route2json('root');
        $this->data = [
          'title'   =>  'Yeni Üye'
        ];
        return $this->b_view('users.create');
    }
}
