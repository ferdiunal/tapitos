<?php

namespace App\Http\Controllers\Back\Users;

use App\Http\Requests\Root\Users\Update;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\Paginator;

class All extends Controller
{
    public function index()
    {  
        $appends = [];
        $baseURL = strtok(request()->fullUrl(),'?');
        $users = $this->users();
        if(request()->has('role')){
            if(request()->get('role') === 'tumu' || request()->get('role') === ""){

                if(request()->has('status')){
                    return redirect()->route('root@user.all',[ 'status' => request()->get('status') ]);
                }
                return redirect()->route('root@user.all');

            }
            $roleQuery = \Sentinel::getRoleRepository()->createModel()->where('slug','=',request()->get('role'));
            if(!$roleQuery->count()){
                if(request()->has('status')){
                    return redirect()->route('root@user.all',[ 'status' => request()->get('status') ]);
                }
                return redirect()->route('root@user.all');
            }
            $users = $roleQuery->first()->users()->simplePaginate(15);
            $appends['role'] = request()->get('role');
        }
        if(request()->has('status')){
            if(request()->get('status') === 'tumu' || request()->get('status') === "" ){

                if(request()->has('role')){
                    return redirect()->route('root@user.all',[ 'role' => request()->get('role') ]);
                }
                return redirect()->route('root@user.all');

            }
            if(request()->get('status') === 'etkin'){
                $qStatus = 1;
            } else if(request()->get('status') === 'pasif'){
                $qStatus = 0;
            }

            $statusQuery = \Sentinel::getActivationRepository()
                                    ->createModel()
                                    ->where('user_id','!=',1)
                                    ->where('completed','=',$qStatus);
            if(!$statusQuery->count()){
                if(request()->has('role')){
                    return redirect()->route('root@user.all',[ 'role' => request()->get('role') ]);
                }
                return redirect()->route('root@user.all');
            }

            $tmpUsers = [];
            foreach ($statusQuery->orderBy('user_id','desc')->get() as $status) {
                $tmpUsers[] = \Sentinel::findById($status->user_id);
            }
            $tmpUsers = new Paginator($tmpUsers,15);
            $users = $tmpUsers;
            $appends['status'] = request()->get('status');
        }
        if(int(request()->has('search'))){

            $search = request()->get('search');

            $qSearch = explode(',',$search);

            if(count($qSearch) > 0){
                $user = \Sentinel::getUserRepository()->createModel()->where('id','!=',1)->whereIn('email',array_map('trim',$qSearch));

            } else {
                $user = \Sentinel::getUserRepository()->createModel()->where('id','!=',1)->where('email','=',$search);
            }


            if(!$user->count()){
                return redirect()->route('root@user.all');
            }

            $users = new Paginator($user->get(),15);

            $appends['search'] = $search;
        }
        
        $users->setPath($baseURL);
        
        
        $this->data = [
            'title'   =>  'Tüm Üyeler',
            'users'  =>  $users,
            'appends'   =>  $appends
        ];
        $view =  $this->b_view('users.all');

        if(request()->has('showUser')){
            $view->nest('showUser','Back::users.assets.showUser');
        }

        if(request()->has('updateUser')){
            if(request()->get('updateUser') === '1'){
                return redirect()->route('root@user.all');
            }
            $getUser = \Sentinel::findById(request()->get('updateUser'));
  
            $view->nest('updateUser','Back::users.assets.updateUser',['user' => $getUser]); 
        }

        return $view;
    }

    public function users()
    {
        return \Sentinel::getUserRepository()->createModel()->where('id','!=',1)->orderBy('id','desc')->simplePaginate(15);
    }

    public function update(Update $update)
    {
        $user = \Sentinel::findById($update->get('user_id'));
        if(!$user){
            return response([
                'error' =>  'Güncelleme esnasında bir problem oluştu lütfen tekrar deneyin'
            ],423);
        }

        $currRole = $user->roles()->first()->slug;

        if($update->get('role') !== $currRole){
            $role = \Sentinel::findRoleBySlug($currRole);
            $role->users()->detach($user);
            $updateRole = \Sentinel::findRoleBySlug($update->get('role'));
            $updateRole->users()->attach($user);
        }

        $model = \Sentinel::getUserRepository()->createModel();
        $checkMail = $model->where('id','!=',$update->get('user_id'))->where('email','=',$update->get('email'));
        if($checkMail->count() > 0){
            return response([
                'email' =>  $user->email,
                'error' =>  'Girmiş olduğunuz eposta adresi daha önce kullanılmış.'
            ],423);
        } else if(!$checkMail->count()){
            $model->where('id','=',$update->get('user_id'))->update($update->except(['role','user_id']));
        }

        return response([
            'Güncelleme başarılı.'
        ]);
    }

    public function delete($user_id)
    {
        $user = \Sentinel::findById($user_id);
        $user->delete();
        return redirect()->route('root@user.all');

    }
}
