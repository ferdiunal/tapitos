<?php

namespace App\Http\Controllers\Back\Roles;

use App\Http\Requests\Root\Roles\Permissions;
use App\Http\Requests\Root\Users\Update;
use Illuminate\Http\Request as Http;

use App\Http\Controllers\Controller;


class Create extends Controller
{
    public function index(Http $http)
    {
        $this->data = [
            'title'   =>  'Yeni Rol'
        ];

        $view = $this->b_view('users.roles');
        if( $http->has('roleId') ) {
            $roleId = (int)$http->get('roleId');
            if ($roleId === 1) {
                return redirect()->route('root@user.role');
            }
            $getRole = \Sentinel::findRoleById($roleId);
            if (!$getRole) {
                return redirect()->route('root@user.role');
            }
            $this->data['getRole'] = $getRole;
        }
        if($http->has('roleId') && $http->has('do')){
            switch ($http->get('do')){
                case "forceDelete" :
                    $getRole->delete();
                    return redirect()->route('root@user.role');
                    break;
                case "delete" :
                    if($http->has('perm')){
                        $permissions = $getRole->permissions;
                        unset($permissions[$http->get('perm')]);
                        $getRole->permissions = $permissions;
                        $getRole->save();
                        return redirect()->route('root@user.role',['popup' => 'update','roleId' => $getRole->id]);
                    }
                    break;
            }
        }
        if($http->has('popup') && $http->has('roleId')){
            switch ($http->get('popup')){

                case 'update' :
                    $view->nest('role_update','Back::users.assets.updateRole',$this->data);
                    break;

                case 'permissions' :
                    $view->nest('role_permissions','Back::users.assets.permissionsRole',$this->data);
                    break;

            }

        }

        return $view;
    }

    public function update(Http $request)
    {
        $data = $request->except(['_token']);
        $getRole = \Sentinel::findRoleById($data['roleId']);
        if(!$getRole){
            return redirect()->route('root@user.role',['popup' => 'update','roleId' => $data['roleId']]);
        }
        $perms = $data['perm'];
        $diffPerms = [];
        foreach ($getRole->permissions as $k => $v) {
            if(!isset($perms[$k])){
                $diffPerms[$k] = false;
            } else if(isset($perms[$k])){
                $diffPerms[$k] = true;
            }
        }


        $getRole->name = $data['name'];
        $getRole->slug = $data['slug'];
        $getRole->permissions = $diffPerms;
        $getRole->save();
        return redirect()->route('root@user.role',['popup' => 'update','roleId' => $data['roleId']]);
    }

    public function create(Http $request)
    {
        if(str_slug($request->get('name')) === 'root' || $request->get('slug') === 'root'){
            return redirect()->route('root@user.role');
        }
        $data = $request->except(['_token']);
        $isRole = \Sentinel::findRoleBySlug($data['slug']);

        if($isRole){
            return redirect()->route('root@user.role');
        }

        \Sentinel::getRoleRepository()->create($data);
        return redirect()->route('root@user.role');

    }
    
    public function store(Permissions $request)
    {
        $popup = $request->get('popup');
        $roleId = (int) $request->get('roleId');
        $getRole = \Sentinel::findRoleById($roleId);
        if(!$getRole ||$roleId === 1){
            return redirect()->route('root@user.role');
        }

        switch ($popup){

            case 'permissions' :
                $active = $request->has('permission_status');
                $perm = $request->get('permission_name');

                $permission = [ $perm => $active ];

                $oldPermission = $getRole->permissions;
                if(!count($oldPermission)){
                    $getRole->permissions = $permission;
                }
                if(count($oldPermission) > 0){
                    if((in_array($perm,$oldPermission)) === true){
                        $oldPermission[$perm] = $active;
                    }
                    
                    $getRole->permissions = $oldPermission;
                }

                $getRole->save();
                return redirect()->route('root@user.role',['popup' => 'update','roleId' => $roleId]);
                break;
        }

    }
}