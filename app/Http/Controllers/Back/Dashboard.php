<?php

namespace App\Http\Controllers\Back;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Plugins\Root\Sidebar;

class Dashboard extends Controller
{
    public function index()
    {
        $this->data = [
            'title'     =>  'Anasayfa'
        ];
        return $this->b_view('dashboard');
    }
}
