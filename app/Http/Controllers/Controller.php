<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $data = [];

    protected function view($view = null, $data = [], $mergeData = [])
    {
        return view($view, $data, $mergeData)
                    ->nest('header','Front::assets.header')
                    ->nest('footer','Front::assets.footer')
        ;
    }

    protected function b_view($view = null)
    {
        return view('Back::' . $view, $this->data)
            ->nest('header','Back::assets.header',$this->data)
            ->nest('sidebar','Back::assets.sidebar',$this->data)
            ;
    }
}
