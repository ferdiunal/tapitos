<?php
/**
 * Created by PhpStorm.
 * User: ferdiunal
 * Date: 21.03.2016
 * Time: 10:45
 */
Route::group([ 'middleware' => 'guest','group_title' => 'Oturum' ],function(){
    Route::group([ 'prefix' => 'giris-yap'],function(){
        Route::get('/',['as' => 'auth@login','uses' => 'Login@index','title' => 'Giriş Yap']);
        Route::post('/','Login@store');
        Route::post('/pass_code','Login@pass_code');
    });

    Route::get('/sifre-sifirla',['as' => 'auth@reminder','uses' => 'Reminder@index','title' => 'Şifreni mi unuttun ?']);
    Route::post('/sifre-sifirla','Reminder@store');

    Route::get('uye-ol', [ 'as' => 'auth@register', 'uses' => 'Register@index','title' => 'Üye ol']);
    Route::post('uye-ol','Register@store');
});
Route::get('cikis',['as' => 'auth@logout','uses' => 'Login@logout','title' =>   'Çıkış Yap']);
Route::group(['prefix' => 'verify'],function(){
    Route::get('a@{id}-{code}',['as' => 'verify@activated','uses' => 'Verify@activated'])->where('code','[A-Za-z0-9]{32}')->where('id','\d+');
    Route::get('r@{id}-{code}',['as' => 'verify@reminder','uses' => 'Verify@reminder'])->where('code','[A-Za-z0-9]{32}')->where('id','\d+');
});