<?php
/**
 * Created by PhpStorm.
 * User: ferdiunal
 * Date: 21.03.2016
 * Time: 10:45
 */

Route::group(['prefix'  =>  'root'],function(){

    Route::get('dashboard',['as' => 'back@dashboard', 'uses' => 'Dashboard@index','title' => 'Anasayfa']);

    Route::group(['prefix' =>   'uye', 'group_title' => 'Üyeler'],function(){
        Route::get('yeni',['as' => 'back@user.new','uses' => 'Users\Create@index','title' => 'Yeni']);
        Route::get('tumu',['as' => 'back@user.all','uses' => 'Users\All@index','title' => 'Tümü']);
        Route::get('yetkiler',['as' => 'back@user.role','uses' => 'Roles\Create@index','title' => 'Yetkiler']);
        Route::get('sil-user-id-{id}',['as' => 'back@user.delete','uses' => 'Users\All@delete'])->where('id','\d+');

        Route::post('yetkiler','Roles\Create@store');
        Route::post('yetki-guncelle',['as' => 'back@user.role.update','uses' => 'Roles\Create@update']);
        Route::post('yetki-olustur',[ 'as' => 'back@user.role.create','uses' => 'Roles\Create@create' ]);
        Route::post('update',[ 'as' => 'back@user.update','uses' => 'Users\All@update' ]);
    });
});