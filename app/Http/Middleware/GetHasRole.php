<?php

namespace App\Http\Middleware;

use Closure;

class GetHasRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$role_data = '')
    {

        if(\Sentinel::guest()){
            return redirect()->route('auth@login');
        }
        $permission = [];
        $user = \Sentinel::getUser();
        if(str_contains($role_data,'@')){
            $roleData = explode('@',$role_data);
            $role = $roleData[0];
            $permission = explode(",",$roleData[1]);
        } else if(!str_contains($role_data,'@')){
            $role = $role_data;
        }

        if($user->inRole($role)){

            if(count($permission) > 0 && !$user->hasAccess($permission) && str_contains($role_data,'@')){
                return redirect()->route('home');
            }

            return $next($request);

        }
        return redirect()->route('home');

    }
}
