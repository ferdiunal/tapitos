<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\Request;

class Register extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Sentinel::guest();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name'    =>  'required|min:2|alpha',
            'last_name'     =>  'required|min:2|alpha',
            'email'         =>  'required|email|unique:users,email|confirmed',
            'email_confirmation' =>  'required',
            'password'      =>  'required|between:8,16',
            'terms'         =>  'required'
        ];
    }

    public function messages()
    {
        return [
            'first_name.required'   =>  'Bu alanın doldurulması zorunludur.',
            'first_name.min'        =>  'Adınız en az :min karakter uzunluğunda olmalıdır.',
            'first_name.alpha'      =>  'Adınız harflerden (a-z,A-Z) oluşmalıdır.',
            'last_name.required'    =>  'Bu alanın doldurulması zorunludur.',
            'last_name.min'         =>  'Adınız en az :min haneli olmalıdır.',
            'last_name.alpha'       =>  'Adınız harflerden (a-z,A-Z) oluşmalıdır.',
            'email.required'        =>  'Bu alanın doldurulması zorunludur.',
            'email.email'           =>  'Geçerli bir eposta adresi girin.',
            'email.unique'          =>  'Girmiş olduğunuz eposta adresi kullanılmaktadır. ',
            'email.confirmed'       =>  'Eposta adresiniz tekrarı ile uyuşmamaktadır.',
            'email_confirmation.required'   =>  'Bu alanın doldurulması zorunludur.',
            'password.required'     =>  'Bu alanın doldurulması zorunludur.',
            'password.between'      =>  'Şifreniz en az :min, en fazla :max karakter uzunluğunda olmalıdır.',
            'terms.required'        =>  'Üyelik için Sözleşmemizi ve Şartlarını kabul etmeniz lazım.'
        ];
    }
}
