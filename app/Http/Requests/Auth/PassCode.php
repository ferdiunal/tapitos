<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\Request;

class PassCode extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check() === false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' =>  'required|email',
            'password'  =>  'required|between:8,16',
            'pin_code' =>  'required|numeric'
        ];
    }

    public function messages()
    {
        return [
            'email.required' =>  'Bu alanın doldurulması zorunludur.',
            'email.email'   =>  'Lütfen geçerli bir e-posta adresi giriniz.',
            'password.required' =>  'Bu alanın doldurulması zorunludur.',
            'password.between'  =>  'Şifreniz :min - :max uzunlukta olmalıdır.',
            'pin_code.required'    =>  'Bu alanın doldurulması zorunludur.',
            'pin_code.numeric' =>  'Güvenlik Kodu sayısal olmalıdır.'
        ];
    }
}
