<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\Request;

class Login extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Sentinel::guest();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' =>  'required|email',
            'password'  =>  'required|between:8,16'
        ];
    }

    public function messages()
    {
        return [
            'email.required' =>  'Bu alanın doldurulması zorunludur.',
            'email.email'   =>  'Lütfen geçerli bir e-posta adresi giriniz.',
            'password.required' =>  'Bu alanın doldurulması zorunludur.',
            'password.between'  =>  'Şifreniz :min - :max uzunlukta olmalıdır.'
        ];
    }
}
