<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\Request;

class Reminder extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Sentinel::guest();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' =>  'required|email',
            'g-recaptcha-response'  =>  'required|reCaptcha'
        ];
    }

    public function messages()
    {
        return [
            'email.required'    =>  'Bu alanın doldurulması zorunludur.',
            'email.email'       =>  'Geçerli bir eposta adresi giriniz.',
            'g-recaptcha-response.required' => 'Bu alanın doldurulması zorunludur.',
        ];
    }
}
