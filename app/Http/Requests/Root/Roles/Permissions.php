<?php

namespace App\Http\Requests\Root\Roles;

use App\Http\Requests\Request;

class Permissions extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return !\Sentinel::guest() && \Sentinel::getUser()->inRole('root');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'popup' =>  'required',
            'roleId'    =>  'required',
            'permission_name'   =>  'required',
        ];
    }
}
