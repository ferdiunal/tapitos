<?php

namespace App\Http\Requests\Root\Users;

use App\Http\Requests\Request;

class Update extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return !\Sentinel::guest() && \Sentinel::getUser()->inRole('root');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id'       =>  'required|numeric',
            'first_name'    =>  'required|min:2|alpha_spaces',
            'last_name'     =>  'required|min:2|alpha_spaces',
            'email'         =>  'required|email',
            'role'          =>  'required'
        ];
    }

    public function messages()
    {
        return [
            'user_id.required'      =>  'Kullanıcı id\'si gereklidir !',
            'user_id.numeric'       =>  'Kullanıcı id\'si sayısal olmalıdır',
            'first_name.required'   =>  'Bu alanın doldurulması zorunludur.',
            'first_name.min'        =>  'Adınız en az :min karakter uzunluğunda olmalıdır.',
            'first_name.alpha_spaces'      =>  'Adınız harflerden (a-z,A-Z) oluşmalıdır.',
            'last_name.required'    =>  'Bu alanın doldurulması zorunludur.',
            'last_name.min'         =>  'Soyadınız en az :min haneli olmalıdır.',
            'last_name.alpha_spaces'       =>  'Soyadınız harflerden (a-z,A-Z) oluşmalıdır.',
            'email.required'        =>  'Bu alanın doldurulması zorunludur.',
            'email.email'           =>  'Geçerli bir eposta adresi girin.',
            'role.required'         =>  'Bu alanın doldurulması zorunludur.',
        ];
    }

}
