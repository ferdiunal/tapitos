$(function(){

    function spinner(){
        const spinner = $(".spinner");
        if(spinner.length > 0) {
            const up = spinner.find('.up');
            const down = spinner.find('.down');
            if(up.length && down.length){

                up.on("click",function(){
                    const $this = spinner.find('input');
                    const $val = parseInt($this.val());
                    const $max = parseInt($this.attr('max'));
                    const $step = parseInt($this.attr('step'))

                    if($val > $max){
                        $this.val($max);
                    }
                    if($val >= $max) return false;
                    $this.val($val + $step);
                    $this.trigger('focus');


                });

                down.on("click",function(){
                    const $this = spinner.find('input');
                    const $val = parseInt($this.val());
                    const $min = parseInt($this.attr('min'));
                    const $step = parseInt($this.attr('step'));

                    if($val < $min){
                        $this.val($min);
                    }

                    if($val <= $min) return false;
                    $this.val($val - $step);
                    $this.trigger('focus');
                });

                spinner.find('input').on('change',function(evt){
                    var max = parseInt($(this).attr('max')),
                        min = parseInt($(this).attr('min')),
                        val = parseInt($(this).val())


                    if(val > max || ( val >= max && $(this).val().length !== 3 )){
                        $(this).val(max);
                    }
                    if(val < min || (val < min && $(this).val().length !== 2)){
                        $(this).val(min)
                    }
                    $(this).trigger('focus');

                });
                spinner.find('input').on('keypress',function(evt){
                    evt = (evt) ? evt : window.event;
                    var charCode = (evt.which) ? evt.which : evt.keyCode;

                    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                        return false;
                    }
                    return true;
                })

            }


        }
    }

    function productCarousel($this){
        const $tuhumbl = $('.product-tumbl ul li');
        $tuhumbl.on('click',function(){
            const frame = $(this).data('frame');
            const frame_type = $(this).data('frame_type');
            const product_box = $('#carousel .product-photo');
            $('.product-tumbl ul li.active').removeClass('active');
            $(this).addClass('active');
            switch (frame_type) {
                case "img" :
                    product_box.html('<img src="'+ frame +'?v=1" class="img-responsive">');
                    break;
                case "youtube" :
                    product_box.html('<div class="embed-responsive embed-responsive-16by9" style="min-height: 419px !important;"><iframe class="embed-responsive-item" width="450" height="450" src="'+frame+'?rel=0&amp;controls=0&amp;showinfo=0&amp;autoplay=1&amp;fs=0&amp;modestbranding=1&amp;vq=hd720" frameborder="0" allowfullscreen></iframe></div>');
                    break;
            }

        })
        $.each($tuhumbl,function(i,e){
            if($(this).is('.active')){
                const frame = $(this).data('frame');
                const frame_type = $(this).data('frame_type');
                const product_box = $('#carousel .product-photo');
                switch (frame_type) {
                    case "img" :
                        product_box.html('<img src="'+ frame +'?v=1">');
                        break;
                    case "youtube" :
                        product_box.html('<div class="embed-responsive embed-responsive-16by9" style="min-height: 419px !important;"><iframe class="embed-responsive-item" width="450" height="450" src="'+frame+'?rel=0&amp;controls=0&amp;showinfo=0&amp;autoplay=1&amp;fs=0&amp;modestbranding=1" frameborder="0" allowfullscreen></iframe></div>');
                        break;
                }

            }
        })
    }

    spinner();
    productCarousel();


});