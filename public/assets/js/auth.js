/**
 * Created by ferdiunal on 21.03.2016.
 */
$(function(){

   $('form input').on('keypress',function(){
       window.onbeforeunload = function(){
           return 'Bu işlemi gerçekleştiğinde verileriniz silinecektir !';
       }

       $(this).popover('hide');

   });
});
function popoverError( $this, content, pos) {
    // the trick to "refresh content" in every call
    if(pos === undefined) { pos = 'right' }
    $this.attr('data-content', content);
    // popover
    $this.popover(
        {
            trigger :   'manual',
            container:'body',
            placement: function() { return $(window).width() < 975 ? 'top' : pos; }
        })
        .popover('show').blur(function(){
            $(this).popover('hide')
        });
}
$('[data-toggle="popover"]').popover();
$.ajaxSetup({
    dataType : 'json',
    method    : 'post',
    headers: { 'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') },
    error: function(jqXHR, exception) {
        if (jqXHR.status === 0) {
            alert('Not connect.\n Verify Network.');
        } else if (jqXHR.status == 404) {
            alert('Requested page not found. [404]');
        } else if (jqXHR.status == 500) {
            alert('Internal Server Error [500].');
        } else if (exception === 'parsererror') {
            alert('Requested JSON parse failed.');
        } else if (exception === 'timeout') {
            alert('Time out error.');
        } else if (exception === 'abort') {
            alert('Ajax request aborted.');
        } else {
            alert('Uncaught Error.\n' + jqXHR.responseText);
        }
    }
});

const pinCode = function(){
    const pinCode = $('#pinCode');
    if(pinCode.length){
        setTimeout(function(){
            $('.pincode-input-container .pincode-input-text.first').focus();
        },2);
        pinCode.pincodeInput({
            inputs : 6,
            hideDigits:true,
            complete : function (value, e, errorElement) {
                if(value.length === 6){
                    const loginData = $('form#loginForm').serialize() + '&pin_code=' + value;


                    $.ajax({
                        url : $('form#loginForm').attr('action') + '/pass_code',
                        data : loginData,
                        beforeSend : function () {
                            $('.pincode-input-container .pincode-input-text').attr('disabled',true);
                        },
                        error: function(jqXHR, exception) {
                            window.onbeforeunload = null;
                            switch (jqXHR.status){

                                case 422 :

                                    const passCode = $('.pincode-input-container');
                                    if(jqXHR.responseJSON.pin_code){
                                        popoverError(passCode,jqXHR.responseJSON.pin_code)
                                    }
                                    popoverError(passCode,jqXHR.responseJSON.error);
                                    $('.pincode-input-container .pincode-input-text').attr('disabled',false).val("");

                                    setTimeout(function(){
                                        $('.pincode-input-container .pincode-input-text.first').focus();
                                    },2);

                                    break;

                                case 423 :
                                    window.onbeforeunload = null;
                                    $('#rootPinModal').modal('hide');
                                    $('.pincode-input-container .pincode-input-text').attr('disabled',false).val("");
                                    $('form#loginForm').find('input[name=password]').val("").focus();
                                    $('form#loginForm').prepend('<div class="alert alert-danger text-center">'+ jqXHR.responseJSON.error +'</div>');
                                    break;
                                case 424 :
                                    window.onbeforeunload = null;
                                    $('#rootPinModal,.modal-backdrop,.popover').remove();
                                    $('form').prepend('<div class="alert alert-danger text-center">'+ jqXHR.responseJSON.global_error +'</div>');
                                    break;

                            }

                        },
                        success : function (e) {
                            window.onbeforeunload = null;
                            if(e.redirect){
                                window.onbeforeunload = null;
                                window.location.href = e.redirect
                            }
                        }
                    })

                }
            }
        })
    }
};

const auth = {
    reminder : function(formId){
        const $this = $(formId);
        $.ajax({
            url : $this.attr('action'),
            data : $this.serialize(),
            beforeSend : function () {
                $(formId + ' *').not('a.btn').attr('disabled',true);
                $('body').find('.popover').remove();
                $this.find('.alert').remove();
            },
            success : function (e) {
                window.onbeforeunload = null;
                $(formId + ' *').not('a.btn').attr('disabled',false);
            },
            error: function(jqXHR, exception) {
                $(formId + ' *').attr('disabled',false);
                const res = jqXHR.responseJSON;
                switch (jqXHR.status){
                    case 423 :
                        window.onbeforeunload = null;
                        $this.prepend('<div class="alert alert-danger text-center">'+ res.global_error +'</div>');
                        $this.find('input[name=email]').val('');
                        setTimeout(function(){
                            $this.find('input[name=email]').focus()
                        },2);
                        grecaptcha.reset();
                        break;
                    case 422 :
                        $.each(res,function(i,e){
                            if(i === 'g-recaptcha-response'){
                                var input = $this.find('.' +  i).parent();
                            } else if(i !== 'g-recaptcha-response'){
                                var input = $this.find('input[name='+i+']');
                            }
                            const Alert = 'left';
                            popoverError(input,e[0],Alert);
                        });

                        break;
                }
            }
        });
    },
    register : function(formId){
        const $this = $(formId);
        $.ajax({
            url : $this.attr('action'),
            data : $this.serialize(),
            beforeSend : function () {
                $(formId + ' *').attr('disabled',true);
                $('body').find('.popover').remove();
                $this.find('.alert').remove();
            },
            error: function(jqXHR, exception) {
                $(formId + ' *').attr('disabled',false);
                switch (jqXHR.status){
                    case 422 :
                        $.each(jqXHR.responseJSON,function(i,e){
                            if(i === 'terms'){
                                var input = $this.find('input[name='+i+']').parent().parent();
                            } else {
                                var input = $this.find('input[name='+i+']');
                            }
                            const alert = input.data('alert');
                            popoverError(input,e[0],alert);
                        });

                        break;
                }
            },
            success : function (s) {

                if(s.msg){

                    var alertDir = $('main .clearfix');
                    alertDir.prepend('<div class="row"><div class="col-md-6 col-md-push-3"><div class="alert alert-success">'+s.msg.text+'</div></div></div>');
                    window.onbeforeunload = null;
                    setTimeout(function(){
                        window.location.reload();
                    },1000);

                }

            }
        })

    },
    login : function(formId){
        const $this = $(formId);
        const $alert = $this.data('alert');
        $.ajax({
            url : $this.attr('action'),
            data : $this.serialize(),
            beforeSend : function(){
                $(formId + ' *').attr('disabled',true);
                $('body').find('.popover').remove();
                $this.find('.alert').remove();
            }, 
            error: function(jqXHR, exception) {
                $(formId + ' *').attr('disabled',false);
                switch (jqXHR.status){
                    case 424 :
                        const res = jqXHR.responseJSON;
                        if(res.pin_code_show === true)
                            $('body footer').append(res.pin_code_modal);
                            pinCode();
                            const pinModal = $('#rootPinModal');
                            pinModal.modal('show',$this.data('alert'));
                            $('.modal-backdrop').removeClass('in');
                            window.onbeforeunload = function ()
                            {
                                return "Sayfa tekrar yükenecektir, Bilginiz olsun !";
                            };
                        break;
                    case 423 :
                        $this.find('input[name=password]').val("");

                        setTimeout(function(){
                            $this.find('input[name=password]').focus();
                        },2);

                        $this.prepend('<div class="alert alert-danger text-center">'+ jqXHR.responseJSON.global_error +'</div>');
                        break;
                    case 422 :
                        var is = 0;
                        $.each(jqXHR.responseJSON,function(i,e){
                            popoverError($this.find('input[name='+i+']'),e[0],$alert);
                            if(is === 0) {
                                setTimeout(function(){
                                    $this.find('input[name='+i+']').focus();
                                })
                            }
                            is = is + 1
                        });
                        if(is === 2){
                            setTimeout(function(){
                                $this.find('input[name=email]').focus();
                            })
                        }

                        break;
                }
            }
        }).success(function (e) {
            if(e.redirect){
                window.onbeforeunload = null;
                window.location.href = e.redirect
            }
        });
    }
};