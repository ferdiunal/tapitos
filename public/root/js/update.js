/**
 * Created by ferdiunal on 21.03.2016.
 */
$(function(){

    $('form input').on('keypress',function(){
        window.onbeforeunload = function(){
            return 'Bu işlemi gerçekleştiğinde verileriniz silinecektir !';
        }

        $(this).popover('hide');

    });
});

function popoverError( $this, content, pos) {
    // the trick to "refresh content" in every call
    if(pos === undefined) { pos = 'right' }
    $this.attr('data-content', content);
    // popover
    $this.popover(
        {
            trigger :   'manual',
            container:'body',
            placement: function() { return $(window).width() < 975 ? 'top' : pos; }
        })
        .popover('show').blur(function(){
        $(this).popover('hide')
    });
}

$('[data-toggle="popover"]').popover();

const root = {
    user : {
        update : function (formId) {
            const $this = $(formId);
            const $url = $this.attr('action');
            const $serialize = $this.serialize();
            const $disabled = function ($elm, $stauts) {
                $this.find($elm).attr('disabled',$stauts);
            };
            const $button = $this.find('button[type=submit]');
            $.ajax({
                url : $url,
                data : $serialize,
                dataType : 'json',
                method    : 'post',
                headers: { 'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') },
                beforeSend : function () {
                    $disabled('*:not(a)',true);
                    $button.button('loading')
                },
                success : function () {
                    window.onbeforeunload = null;
                    $disabled('*:not(a)',false);
                },
                error: function(jqXHR, exception) {
                    window.onbeforeunload = null;
                    $disabled('*:not(a)',false);
                    const $errData = jqXHR.responseJSON;
                    const $status = jqXHR.status;
                    switch ($status){
                        case 422 :

                            $.each($errData,function(i,e){
                                const $input = $this.find('input[name='+ i +']');
                                const $popoverPos = $input.data('popover-pos');
                                popoverError($input,e[0],$popoverPos)
                            });
                            console.log($errData);
                            break;
                        case 423 :
                            $this.find('input[name=email]').val($errData.email);
                            $this.parent().prepend('<div class="alert alert-danger">'+ $errData.error    +'</div>');
                            setTimeout(function(){
                                $this.find('input[name=email]').focus();
                            },2);
                            break;
                    }
                }
            });
            setTimeout(function(){
                $button.button('reset');
            },1500);

        //    TODO Uyarı mesajları gösterilecektir.

        }
    }
}