<?php
/**
 * Created by PhpStorm.
 * User: ferdiunal
 * Date: 01.04.2016
 * Time: 16:05
 */

namespace Libraries;


class Route2JSON
{

    public function getRoutes($appName = null)
    {
        $routes = \Route::getRoutes();
        $router = [];
        $group_title = null;
        $title = null;
        $ignore = ['verify','auth'];
        $current = \Route::currentRouteName();
        $tmp_router = [];
        $child = [];
        foreach ($routes as $route) {
            $name = $route->getName();

            if($name){
                $action = $route->getAction();
                $set_module = explode('@',$name);
                if(isset($set_module[1])){
                    $set_app = explode('.',$set_module[1]);
                    $app = $set_app[0];
                    $module = $set_module[0];
                    $userRole = \Sentinel::getUser()->roles()->first()->slug;
                    $getRole = 'role:' . $userRole;

                    if(! in_array($module,$ignore) && in_array($getRole,$action['middleware'])){
                        if(!isset($action['group_title']) && isset($action['title'])){
                            $tmp_router[$module][] = [
                                'title' =>  $action['title'],
                                'url'   =>  route($name),
                                'isActive'  =>  $name === $current
                            ];
                            $router[$module]['single'] = $tmp_router[$module];
                        }
                        if(isset($action['group_title']) && isset($action['title'])){

                            $child[$module][] = [
                                'title' =>  $action['title'],
                                'url'   =>  route($name),
                                'isActive'  =>  $name === $current
                            ];
                            $current_prefix = explode('.',$name)[0];
                            $router[$module]['group'][$app] = [
                                'title' =>  $action['group_title'],
                                'current'   =>  str_contains($current,$current_prefix),
                                'child' => $child[$module]
                            ];
                        }


                    }

                }
            } 
        }


        if($appName !== null && isset($router[$appName])){
            $router[$appName]['group'] = array_values($router[$appName]['group']);

            return $router[$appName];
        }

        return $router;
    }

}