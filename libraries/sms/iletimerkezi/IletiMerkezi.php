<?php
/**
 * Created by PhpStorm.
 * User: ferdiunal
 * Date: 22.03.2016
 * Time: 23:30
 */

namespace IletiMerkezi;


class IletiMerkezi
{

    protected $app;
    protected $http;
    /**
     * IletiMerkezi constructor.
     * @param $app
     */
    public function __construct($app,$http)
    {
        $this->app = $app;
        $this->http = $http;
    }

    protected function config($config_name = "")
    {
        if($config_name === "") return false;
        return $this->app->config->get('IletiMerkezi.' . $config_name);
    }

    protected function setRequestXML($receipents = "",$text="")
    {
        return "
            <request>
                <authentication>
                    <username>{$this->config('username')}</username>
                    <password>{$this->config('password')}</password>
                </authentication>
                
                <order>
                    <sender>{$this->config('sender')}</sender>
                    <message>
                         <text>{$text}/text>
                         <receipents>
                             <number>{$receipents}</number>
                         </receipents>
                    </message>
                </order>
            </request>
   		 ";
    }

    protected function setRequest($receipents = "",$text="")
    {
        $url = $this->config('url') . '/get/';
        $username   = '&username=' . $this->config('username');
        $password   = '&password=' . $this->config('password');
        $sender     = '&sender=' . $this->config('sender');
        $text       = '&text=' . $text;
        $receipents = '&receipents=' . $receipents;

        return $url . $username . $password . $sender . $text . $receipents;

    }
    
    public function sendSMS()
    {
        $request = $this->setRequest('5459636588',"Test SMS");
        $sms = $this->http->request('GET',$request);
        return $sms->getStatusCode();
    }

}