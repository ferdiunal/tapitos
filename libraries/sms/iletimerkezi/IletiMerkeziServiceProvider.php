<?php
/**
 * Created by PhpStorm.
 * User: ferdiunal
 * Date: 22.03.2016
 * Time: 23:30
 */

namespace IletiMerkezi;


use Illuminate\Support\ServiceProvider;
use GuzzleHttp\Client;
class IletiMerkeziServiceProvider extends ServiceProvider
{


    public function boot(){

        $this->mergeConfigFrom(__DIR__ . '/config.php','IletiMerkezi');

    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $http = new Client();
        $this->app->bindIf('sms',function($app) use($http){
            return new IletiMerkezi($app,$http);
        });
    }

    public function provides()
    {
        return [ IletiMerkezi::class ];
    }
}