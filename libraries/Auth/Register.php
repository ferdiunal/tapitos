<?php
/**
 * Created by PhpStorm.
 * User: ferdiunal
 * Date: 25.03.2016
 * Time: 15:47
 */

namespace Libraries\Auth;


use Cartalyst\Support\Mailer;

trait Register
{


    /*
     * TODO Eposta adresine hesap etkkinleştirme mail'i gönderilecek.
     * TODO MailChimp iyice araştırılacak.
     * TODO MailChimp kütüphanesi oluşturulacak.
     */
    public function register($credentials = [])
    {

        $user = \Sentinel::register($credentials);
        $activated = \Activation::create($user);
        $role_user = \Sentinel::getRoleRepository()->findBySlug('user');
        $role_user->users()->attach($user);
        $email = $user->email;
        $display_name = $user->first_name . ' '. $user->last_name;
        $activated_code = $activated['code'];
        $activated_url = route('verify@activated',['code' => $activated_code,'id' => $user->id]);

        $mail_data = [
            'display_name'  =>  $display_name,
            'email'         =>  $email,
            'activated_url' =>  $activated_url
        ];
        $this->sendMail($mail_data);

//        \Sentinel::loginAndRemember($user);

        return response([
            'msg'   =>  [
                'type'  =>  'success',
                'text'  =>  'Hesabınız başarılı bir şekilde oluşturuldu, Lütfen eposta adresinizi kontrol ediniz.'
            ]
        ]);

    }

    private function sendMail($mail_data = [])
    {
        $mail_view = 'Front::email.activated';
        \Mail::send($mail_view,$mail_data,function($m)use($mail_data){
            $m->to($mail_data['email']);
//            $m->bcc('ferdi@unal.pw');
        });
    }

}