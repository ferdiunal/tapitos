<?php
/**
 * Created by PhpStorm.
 * User: ferdiunal
 * Date: 25.03.2016
 * Time: 09:45
 */

namespace Libraries\Auth;


use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Cartalyst\Support\ServiceProvider;

class oAuthProvider extends ServiceProvider
{

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bindIf('oAuth',function($app){
            return new oAuth($app);
        });
    }
}