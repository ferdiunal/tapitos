<?php
/**
 * Created by PhpStorm.
 * User: ferdiunal
 * Date: 25.03.2016
 * Time: 10:54
 */

namespace Libraries\Auth;
use Cartalyst\Sentinel\Checkpoints\{
    ThrottlingException,NotActivatedException
};
trait Login
{
    private $credentials = [];



    private function get_pass_code_modal()
    {
        return \File::get(resource_path('views/front_end/assets/pass_code.html'));
    }

    private function get_pass_code()
    {
        return $this->credentials->pass_code;
    }

    private function modal_pass_code()
    {
        if ($this->get_pass_code()) {
            return response([
                'pin_code_show' =>  true,
                'pin_code_modal'    =>  $this->get_pass_code_modal()
            ],424);
        }
        return false;
    }
    public function login($credentials = [], $reminder = false)
    {
        try{
            $this->credentials = $credentials;
            $auth = \Sentinel::authenticate($this->credentials,$reminder);

            if($auth){
                $this->credentials = $auth;
                if($this->get_pass_code()){
                    \Sentinel::logout($auth);
                    return $this->modal_pass_code();
                }
                return [ 'redirect' => route('root@dashboard') ];
            }
            return response([ 'global_error'    =>  "Eposta adresin yada Şifren hatalı." ],423);
        } catch (ThrottlingException $e){
            return response([ 'global_error'    =>  "Birden fazla hatalı giriş denediniz, Lütfen {$e->getDelay()} sn bekleyiniz." ],423);
        } catch (NotActivatedException $e){
            return response([ 'global_error'    =>  "Hesabınızı etkinleştirmeniz gerekmektedir." ],423);
        }
    }

    public function pass_code($credentials = [], $reminder = false)
    {
        if(!isset($credentials['pin_code'])){
            return response([
                'global_error'  =>  'Güvenlik Kodu Gerekli !'
            ]);
        }

        $this->credentials = $credentials;
        $pin_code = $this->credentials['pin_code'];


        $this->credentials = $credentials;
        $auth = \Sentinel::authenticate($this->credentials,$reminder);

        if($auth){
            $this->credentials = $auth;
            if($this->get_pass_code()){

                if(!$this->hasher->check($pin_code,$this->get_pass_code())){
                    \Sentinel::logout($auth);
                    return response(['error' => 'Güvenlik Kodu Hatalı ! '],422);
                }
                return response([ 'redirect' => route('root@dashboard') ]);

            }
        }
        return response([ 'global_error'   =>  'Giriş esnasında bir problem oluştu','pin_code' =>false ],424);
    }

}