<?php
/**
 * Created by PhpStorm.
 * User: ferdiunal
 * Date: 28.03.2016
 * Time: 10:13
 */

namespace Libraries\Auth;


use Carbon\Carbon;
use Cartalyst\Sentinel\Users\UserInterface;
use Cartalyst\Support\Traits\RepositoryTrait;

use Cartalyst\Sentinel\Throttling\IlluminateThrottleRepository;
class Throttle extends IlluminateThrottleRepository
{

    public function log($ipAddress = null, UserInterface $user = null)
    {
        $global = $this->createModel();
        $global->fill([
            'type' => 'global',
            'ip'   => $ipAddress,
        ]);
        $global->save();

        if ($ipAddress !== null) {
            $ipAddressThrottle = $this->createModel();
            $ipAddressThrottle->fill([
                'type' => 'ip',
                'ip'   => $ipAddress,
            ]);
            $ipAddressThrottle->save();
        }

        if ($user !== null) {
            $userThrottle = $this->createModel();
            $userThrottle->fill([
                'type' => 'user',
                'ip'   => $ipAddress,
            ]);
            $userThrottle->user_id = $user->getUserId();
            $userThrottle->save();
        }
    }

}