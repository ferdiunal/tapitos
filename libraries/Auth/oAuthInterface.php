<?php
/**
 * Created by PhpStorm.
 * User: ferdiunal
 * Date: 25.03.2016
 * Time: 10:55
 */

namespace Libraries\Auth;


interface oAuthInterface
{
    /**
     * @return mixed
     */
    public function seeder();

    /**
     * @param array $credentials
     * @param bool $reminder
     * @return mixed
     */
    public function login($credentials = [], $reminder = false);

    /**
     * @param $credentials
     * @return mixed
     */
    public function register($credentials = []);
}