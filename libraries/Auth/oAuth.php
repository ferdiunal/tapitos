<?php
/**
 * Created by PhpStorm.
 * User: ferdiunal
 * Date: 25.03.2016
 * Time: 09:38
 */

namespace Libraries\Auth;


use App\Models\Users;
use Cartalyst\Sentinel\Hashing\Sha256Hasher;
use Sentinel;

class oAuth implements oAuthInterface
{
    use Login,Register;

    /**
     * @var
     */
    protected $app;
    /**
     * @var Sha256Hasher
     */
    protected $hasher;

    /**
     * Login constructor.
     * @param $sentinel
     */
    public function __construct($app)
    {
        $this->app = $app;
        $this->hasher = new Sha256Hasher();
        Sentinel::setHasher($this->hasher);
        Sentinel::getUserRepository(Users::class);
    }


    /**
     * @return $this
     */
    public function setRoles()
    {
        if(!Sentinel::getRoleRepository()->findBySlug('root')){
            Sentinel::getRoleRepository()->createModel()->create([
                'name' => 'Root',
                'slug' => 'root',
                'permissions' => [
                    "*"
                ]
            ]);
        }
        if(!Sentinel::getRoleRepository()->findBySlug('user')){
            Sentinel::getRoleRepository()->createModel()->create([
                'name'  =>  'User',
                'slug'  =>  'user',
                'permissions' => [
                    "user.create" => false,
                    "user.delete" => false,
                    "user.view"   => true,
                    "user.update" => false
                ]
            ]);
        }
        return $this;
    }

    /**
     * @param $credentials
     * @return mixed
     */
    private function check($credentials = [])
    {
        return \Sentinel::findByCredentials($credentials);
    }

    /**
     * @return $this
     */
    private function setRoot()
    {
        $credentials = [
            'first_name'    =>  'Ferdi',
            'last_name' =>  'ÜNAL',
            'email' =>  'ferdi@unal.pw',
            'password'  =>  '123456789',
            'pass_code' => $this->hasher->hash('123456'),
        ];
        if(!$this->check($credentials)){
            $role = Sentinel::getRoleRepository()->findBySlug('root');
            $root = Sentinel::registerAndActivate($credentials);
            $role->users()->attach($root);
        }
        return $this;
    }

    /**
     * @return $this
     */
    private function setUser()
    {

        $credentials = [
            'first_name'    =>  'Ferdi',
            'last_name'     =>  'ÜNAL',
            'email'         =>  'user@unal.pw',
            'password'      =>  '879685847'
        ];
        if(!$this->check($credentials)){
            $role = Sentinel::getRoleRepository()->findBySlug('user');
            $root = Sentinel::registerAndActivate($credentials);
            $role->users()->attach($root);
        }
        return $this;
    }

    /**
     * @return $this
     */
    public function seeder()
    {
        return $this->setRoles()->setRoot()->setUser();
    }

    public function faker()
    {
        $faker = \Faker\Factory::create('en');
        for ($i=0;$i < 100; $i++){ 
            $this->register([
                'first_name'    =>  $faker->firstNameMale,
                'last_name'    =>  $faker->lastName,
                'email' => $faker->safeEmail,
                'password' => bcrypt(str_random(10)),
            ]);

        }
    }
}