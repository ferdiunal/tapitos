<?php
/**
 * Created by PhpStorm.
 * User: ferdiunal
 * Date: 01.04.2016
 * Time: 22:16
 */
use Carbon\Carbon;
use Libraries\Route2JSON;


if( ! function_exists('route2json')){
    /**
     * @return array
     */
    function route2json($app = null){
        $route2json = new Route2JSON();
        return $route2json->getRoutes($app);
    }
}

if( ! function_exists('carbon')){
    /**
     * @param $value
     * @return static
     */
    function carbon($value){

        if (! $value instanceof Carbon) {
            $value = (is_numeric($value)) ? Carbon::createFromTimestamp($value) : Carbon::parse($value);
        }

        return $value;
    }
}

if( ! function_exists('gravatar')){
    /**
     * Return a gravatar image
     *
     * @param  string  $email
     * @param  integer $size
     * @return string
     */
    function gravatar($email, $size = null)
    {
        $url = "https://www.gravatar.com/avatar/" . e(md5(strtolower($email)));

        if ($size) {
            $url .= '?s=' . $size;
        }

        return $url;
    }
}


if( ! function_exists('bool')){
    /**
     * Returns a real boolean from a string based boolean
     *
     * @param string $value
     * @return bool
     */
    function bool($value)
    {
        return ! in_array(strtolower($value), ['no', 'false', '0', '', '-1']);
    }
}

if( ! function_exists('int')){
    /**
     * Return a real integer from a string based integer
     *
     * @param string $value
     * @return int
     */
    function int($value)
    {
        return intval($value);
    }
}

if( ! function_exists('plugins_path')){
    /**
     * @param null $path
     * @return string
     */
    function plugins_path($path = null){
        if($path){
            return base_path('plugins/' . $path );
        }
        return base_path('plugins');
    }
} 