<!DOCTYPE html>
<html lang="tr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="_token" content="{!! csrf_token() !!}">
    <link rel="shortcut icon" href="{!! url('favicon.png') !!}">
    <title>tapitos Şekerleme</title>
    <link href='https://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{!! url('assets/css/font-awesome.min.css') !!}">
    <link rel="stylesheet" href="{!! url('assets/css/bootstrap.min.css') !!}">
    <link rel="stylesheet" href="{!! url('assets/css/helpers.css') !!}">
    <link rel="stylesheet" href="{!! url('assets/css/apps.css') !!}">
    <!--[if lt IE 9]>
        <script src="{!! url('assets/js/html5shiv.min.js') !!}"></script>
        <script src="{!! url('assets/js/respond.min.js') !!}"></script>
    <![endif]-->
    <script src="{!! url('assets/js/modernizr.js') !!}"></script>
    @yield('styles')
</head>
<body>
    <!--[if lt IE 9]>
        <div class="alert alert-danger text-center" style="margin-top: 70px;">
            <strong>Uyarı</strong>
            Tarayıcınız güncel değil, Güncel bir tarayıcı ile giriş yapınız !
        </div>
    <![endif]-->

    @yield('header')
    <main class="controller">
        @yield('main')
    </main>
    @yield('footer')

    <script src="{!! url('assets/js/jquery-2.2.1.min.js') !!}"></script>
    <script src="{!! url('assets/js/bootstrap.min.js') !!}"></script>
    <script src="{!! url('assets/plugins/bootstrap-select/bootstrap-select.min.js') !!}"></script>
    <script src="{!! url('assets/plugins/bootstrap-select/defaults-tr_TR.min.js') !!}"></script>
    <script src="{!! url('assets/js/plugins.js') !!}"></script>
    @yield('scripts')
</body>
</html>