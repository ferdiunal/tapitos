@extends('Front::layout')
@section('main')
    <div class="clearfix">
        <div class="col-md-4 col-md-push-4 col-lg-4 col-lg-push-4 col-sm-12 col-xs-12">
            @yield('loginForm',['dataAlert' => 'right'])
        </div>
    </div>
    @stop
@section('scripts')
    <script src="{!! url('assets/plugins/bootstrap-pincode-input/js/bootstrap-pincode-input.js') !!}"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script src="{!! url('assets/js/auth.js') !!}"></script>
    @stop
@section('styles')
    <link rel="stylesheet" href="{!! url('assets/plugins/bootstrap-pincode-input/css/bootstrap-pincode-input.css') !!}">
    @stop