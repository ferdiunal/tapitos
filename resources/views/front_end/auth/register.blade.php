@extends('Front::layout')
@section('main')
    <div class="clearfix">
        <div class="col-md-5 col-lg-5 col-md-push-1 col-sm-12 col-xs-12">
            <form action="{!! request()->getUri() !!}" method="post" id="registerForm" role="form" autocomplete="off" onautocomplete="return false" novalidate onsubmit="return false">
                <div class="form-group">
                    <legend>Üyelik Formu</legend>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="first_name">Adınız</label>
                            <input type="text" class="form-control input-lg" id="first_name" autofocus data-alert="top" name="first_name">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="last_name">Soyadınız</label>
                            <input type="text" class="form-control input-lg" id="last_name" data-alert="right" name="last_name">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="email-register">Eposta Adresiniz</label>
                            <input type="email" class="form-control input-lg" id="email-register" data-alert="bottom" name="email">
                        </div>
                    </div>
                    <div class="col-md-6">

                        <div class="form-group">
                            <label for="email-register_confirmed">Eposta Adresinizi Tekrar Girin</label>
                            <input type="email" class="form-control input-lg" id="email-register_confirmed" data-alert="right" name="email_confirmation">
                        </div>

                    </div>
                </div>

                <div class="form-group">
                    <label for="password">Şifreniz </label>
                    <input type="password" class="form-control input-lg" id="password" data-alert="right" name="password">
                </div>

                <div class="form-group xs-pt-15">
                    <div class="checkbox">
                        <label>
                                <input type="checkbox" name="terms" data-alert="bottom" onchange="$(this).change(function(){ $(this).parent().parent().popover('hide') })" id="terms"><span class="checkbox-material"><span class="check"></span></span>
                        </label>
                        <a href="#" tabindex="-1" class="text-capitalize f-bold">Üyelik Sözleşmesi şartlarını okudum ve kabul ediyorum.</a>
                    </div>
                </div>

                <div class="form-group">
                    <button class="btn btn-default btn-lg btn-block text-uppercase" type="submit" onclick="auth.register('#registerForm')">Üye ol</button>
                </div>
            </form>
        </div>
        <div class="col-md-4 col-lg-4 col-md-push-2">
            @yield('loginForm')
        </div>
    </div>
    @stop
@section('scripts')
    <script src="{!! url('assets/plugins/bootstrap-pincode-input/js/bootstrap-pincode-input.js') !!}"></script>
    <script src="{!! url('assets/js/auth.js') !!}"></script>
    @stop
@section('styles')
    <link rel="stylesheet" href="{!! url('assets/plugins/bootstrap-pincode-input/css/bootstrap-pincode-input.css') !!}">
    @stop