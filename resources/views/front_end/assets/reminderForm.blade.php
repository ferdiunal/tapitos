@section('loginForm')
<form action="{!! route('auth@reminder') !!}" data-alert="{!! $dataAlert !!}" method="post" id="reminderForm" role="form" autocomplete="off" onautocomplete="return false" novalidate onsubmit="return false">
    <div class="form-group">
        <legend>{!! $title !!}</legend>
    </div>
    <div class="form-group">
        <label for="email">Eposta Adresin</label>
        <input type="email" class="form-control input-lg" id="email" name="email" autofocus>
    </div>
    <div class="form-group">
        <div class="g-recaptcha" data-theme="dark"  data-sitekey="{!! config('recaptcha.site_key') !!}"></div>
    </div>
    <div class="form-group clearfix">
        <a href="{!! route('auth@register') !!}" class="btn btn-danger pull-right btn-lg text-uppercase c-white f-bold">Ücretsiz üye ol</a>
        <button class="btn btn-default btn-lg text-uppercase" type="submit" onclick="auth.reminder('#reminderForm')">Şifreni Sıfırla</button>
    </div>
</form>
@stop
{{-- TODO recaptcha hazırlanacak. --}}
<script src="https://www.google.com/recaptcha/api.js?hl=tr"
        async defer>
</script>
