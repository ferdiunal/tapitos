@section('loginForm')
<form action="{!! route('auth@login') !!}" data-alert="{!! $dataAlert !!}" method="post" id="loginForm" role="form" autocomplete="off" onautocomplete="return false" novalidate onsubmit="return false">
    <div class="form-group">
        <legend>{!! $title !!}</legend>
    </div>
    <div class="form-group">
        <label for="email">Eposta Adresin</label>
        <input type="email" class="form-control input-lg" id="email" name="email" autofocus>
    </div>
    <div class="form-group">
        <label for="password-login">Şifren </label>
        <input type="password" class="form-control input-lg" id="password-login" name="password">
    </div>
    <div class="form-group">
        <a href="{!! route('auth@reminder') !!}" class="pull-right f-bold" style="margin-top: 3px;" tabindex="-1">Şifreni mi unuttun ?</a>
        <div class="checkbox"style="display: inline-block !important;margin: 0 !important;">
            <label>
                <input type="checkbox" name="rememberMe"><span class="checkbox-material"><span class="check"></span></span> Beni Hatırla
            </label>
        </div>
    </div>
    <div class="form-group">
        <button class="btn btn-default btn-lg btn-block text-uppercase" type="submit" onclick="auth.login('#loginForm')">üye girişi</button>
    </div>
</form>
@stop