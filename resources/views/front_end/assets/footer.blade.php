@section('footer')
    <footer class="container">
        <hr>
        <div class="row">
            <ul class="list-inline col-sm-8 col-xs-12">
                <li class="xs-ml-20 sm-ml-0 md-ml-0 lg-ml-0">
                    <ul class="list-unstyled">
                        <li class="title">TapiTos.com</li>
                        <li><a href="#">Hakkımızda</a></li>
                        <li><a href="#">Kullanıcı sözleşmesi</a></li>
                        <li><a href="#">SSS | Destek</a></li>
                        <li><a href="#">Sipariş Takip</a></li>
                        <li><a href="#">İletişim</a></li>
                    </ul>
                </li>
                <li class="md-ml-40 xs-ml-20">
                    <ul class="list-unstyled">
                        <li class="title">Ödeme</li>
                        <li>Banka Kampanyaları</li>
                        <li>Ödeme seçenekleri</li>
                        <li class="title">Kurumsal</li>
                        <li>İş ortaklığı</li>
                        <li>Kurumsal satış</li>
                    </ul>
                </li>
                <li class="md-ml-40 xs-ml-20">
                    <ul class="list-unstyled">
                        <li class="title">Bizi Takip Edin</li>
                        <li>
                            <a href="#">
                                <span class="fa fa-facebook-official"></span>
                                Facebook
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span class="fa fa-twitter"></span>
                                Twitter
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span class="fa fa-instagram"></span>
                                Instagram
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span class="fa fa-google-plus"></span>
                                Google+
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span class="fa fa-pinterest-p"></span>
                                Pinterest
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="md-ml-40 xs-ml-50">
                    <ul class="list-unstyled">
                        <li class="title">Kategoriler</li>
                        <li><a href="#">Mevlüt Şekeri</a></li>
                        <li><a href="#">Nikah Şekeri</a></li>
                        <li><a href="#">Sünnet Şekeri</a></li>
                        <li><a href="#">Bebek Şekeri</a></li>
                        <li><a href="#">Kapı Süsleri</a></li>
                    </ul>
                </li>
            </ul>
            <div class="col-sm-4 col-xs-12">
                <label class="text-capitalize fa-lg" for="bullent_email">
                    <i class="fa fa-envelope-o"></i>
                    Bültene kayıt ol
                </label>

                <div class="input-group">
                    <input type="email" class="form-control input-lg" id="bullent_email" placeholder="Eposta adresiniz">
                  <span class="input-group-btn">
                      <button class="btn btn-default btn-lg btn-block">
                          Kaydet
                      </button>
                  </span>
                </div><!-- /input-group -->
            </div>
        </div>
    </footer>
    @stop