@section('header')
    <header>
        <div class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header col-md-3 col-sm-12">
                    <a {{--style="background: url('{!! url('logo.png') !!}');width:300px; height: 62px;"--}} href="index.html" class="navbar-brand md-mt-5 xs-mt-0 sm-mt-0">
                        <img src="{!! url("candy.png") !!}">
                        tapitos
                    </a>
                    <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main" style="margin-right: 0">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class=" col-md-6 col-sm-12">
                    <div class="navbar-collapse no-transition collapse" id="navbar-main" style="text-align: center;">
                        <ul class="nav navbar-nav xs-mt-5 text-uppercase visible-md-inline-block visible-lg-inline-block visible-sm-block visible-xs-block">
                            <li><a href="koleksiyonlar.html">Koleksiyonlar</a></li>
                            <li><a href="kampanyalar.html">KAMPANYALAR</a></li>
                        </ul>
                        <ul class="nav navbar-nav xs-mt-5 hidden-lg hidden-md visible-sm-block visible-xs-block">
                            <li><a href="uyelik.html">ÜYELİK <i class="fa fa-user" style="vertical-align:2px"></i></a></li>
                            <li><a href="/sepet/"> SEPETİM <i class="fa fa-shopping-cart"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 hidden-sm hidden-xs">
                    <ul class="nav navbar-nav xs-mt-5 navbar-right" style="text-align: right; float:none;">
                        {{-- TODO ÜYELİK BUTONU DROPDOWN BUTTON OLACAK VE GİRİŞ,KAYIT GİBİ BAĞLANTILAR GÖSTERİLECEK --}}
                        <li><a href="uyelik.html">ÜYELİK <i class="fa fa-user" style="vertical-align:2px"></i></a></li>
                        <li><a href="/sepet/"> SEPETİM <i class="fa fa-shopping-cart" style="font-size:20px;"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
        {{--<div class="navbar-notify xs-p-15 md-p-15 sm-p-15 lg-p-15">
            <p>
                <i class="fa fa-truck"></i>
                &emsp;
                <strong>
                    <i class="fa fa-turkish-lira"></i>
                    <span class="text-success">200</span>
                    ve üzeri tüm siparişlerinizde
                    <span class="text-success">ücretsiz kargo</span>
                </strong>
            </p>
        </div>
        <div class="navbar-notify xs-p-15 md-p-15 sm-p-15 lg-p-15 xs-mt-10 xs-mb-10">
            <p>
                <i class="fa fa-whatsapp fa-2x text-success fa-bold hidden-lg"></i>
                <i class="fa fa-whatsapp fa-lg text-success fa-bold hidden-md hidden-sm hidden-xs"></i>
                <br class="hidden-lg">
                <strong class="fa-lg">
                    <span class="text-success">Destek Hattı : </span>
                    <a href="tel:+905459636588" class="hidden-lg">0545 963 6588</a>
                    <span class="hidden-sm hidden-md hidden-xs">0545 963 6588</span>
                </strong>
            </p>
        </div>--}}
    </header>

    @stop