@section('pass_code')

    <div class="modal" id="rootPinModal" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">PassCode Girin</h4>
                </div>
                <div class="modal-body text-center">
                    <input type="text" id="pinCode" name="pinCode" class="form-control">
                </div>
            </div>
        </div>
    </div>
    {{--TODO pin input'ta boş geçmeyi engellemek lazım.--}}

@stop