<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js sidebar-large lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js sidebar-large lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js sidebar-large lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js sidebar-large"> <!--<![endif]-->

<head>
    <!-- BEGIN META SECTION -->
    <meta charset="utf-8">
    <title>{!! $title !!}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="" name="description" />
    <meta content="themes-lab" name="author" />
    <meta name="_token" content="{!! csrf_token() !!}">
    <link rel="shortcut icon" href="{!! url('root/img/favicon.png') !!}">
    <!-- END META SECTION -->
    <!-- BEGIN MANDATORY STYLE -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,800,700,600&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <link href="{!! url('root/css/icons/icons.min.css') !!}" rel="stylesheet">
    <link href="{!! url('root/css/bootstrap.min.css') !!}" rel="stylesheet">
    <link href="{!! url('root/css/plugins.min.css') !!}" rel="stylesheet">
    <link href="{!! url('root/css/style.min.css') !!}" rel="stylesheet">
    <!-- END  MANDATORY STYLE -->
    <!-- BEGIN PAGE LEVEL STYLE -->
    <link href="{!! url('root/plugins/modal/css/component.css') !!}" rel="stylesheet">
    <!-- END PAGE LEVEL STYLE -->
    <script src="{!! url('root/plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js') !!}"></script>
    @yield('styles')
</head>

<body>
@yield('header')
@yield('main')
</body>
 
<!-- BEGIN MANDATORY SCRIPTS -->
<script src="{!! url('root/plugins/jquery-1.11.js') !!} "></script>
<script src="{!! url('root/plugins/jquery-migrate-1.2.1.js') !!} "></script>
<script src="{!! url('root/plugins/jquery-ui/jquery-ui-1.10.4.min.js') !!} "></script>
<script src="{!! url('root/plugins/jquery-mobile/jquery.mobile-1.4.2.js') !!} "></script>
<script src="{!! url('root/plugins/bootstrap/bootstrap.min.js') !!} "></script>
<script src="{!! url('root/plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js') !!} "></script>
<script src="{!! url('root/plugins/bootstrap-select/bootstrap-select.js') !!} "></script>
<script src="{!! url('root/plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js') !!} "></script>
<script src="{!! url('root/plugins/mmenu/js/jquery.mmenu.min.all.js') !!} "></script>
<script src="{!! url('root/plugins/nprogress/nprogress.js') !!} "></script>
<script src="{!! url('root/plugins/charts-sparkline/sparkline.min.js') !!} "></script>
<script src="{!! url('root/plugins/breakpoints/breakpoints.js') !!} "></script>
<script src="{!! url('root/plugins/numerator/jquery-numerator.js') !!} "></script>
<script src="{!! url('root/plugins/jquery.cookie.min.js" type="text/javascript') !!} "></script>
<!-- END MANDATORY SCRIPTS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{!! url('root/plugins/bootstrap-datepicker/bootstrap-datepicker.js') !!} "></script>
<script src="{!! url('root/plugins/modal/js/classie.js') !!} "></script>
<script src="{!! url('root/plugins/modal/js/modalEffects.js') !!} "></script>
<!-- END  PAGE LEVEL SCRIPTS -->


<script src="{!! url('root/js/application.js') !!} "></script>
@yield('scripts')
</html>