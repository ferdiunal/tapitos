@extends('Back::assets.main')
@section('main_content')

    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12">

            <div class="panel panel-default">

                <div class="panel-body">

                    <div class="row">
                        <div class="col-lg-8 col-sm-8 col-md-8">
                            <select name="role" id="role" style="margin: 0;" title="Rol Seçin">
                                <option class="hidden"></option>
                                @if(request()->has('role'))
                                    <option value="tumu">Tümü</option>
                                @endif
                                @foreach(\Sentinel::getRoleRepository()->createModel()->where('slug','!=','root')->get() as $roles)
                                    <option value="{!! $roles->slug !!}" {!! request()->has('role') ? request()->get('role') === $roles->slug ? 'selected' : null : null !!}>{!! $roles->name !!}</option>
                                @endforeach
                            </select>

                            <select name="status" id="status" title="Durum">
                                <option class="hidden"></option>
                                @if(request()->has('status'))
                                    <option value="tumu">Tümü</option>
                                @endif
                                <option value="etkin" {!! request()->has('status') ? request()->get('status') === 'etkin' ? 'selected' : null : null !!}>Etkin</option>
                                <option value="pasif" {!! request()->has('status') ? request()->get('status') === 'pasif' ? 'selected' : null : null !!}>Pasif</option>
                            </select>
                            <a href="#" class="btn btn-default m-b-10"> <span class="fa fa-trash-o"></span> Çöp Kutusu</a>
                        </div>

                        <div class="col-lg-4 col-sm-4 col-md-4">

                            <form action="{!! strtok(request()->fullUrl(),'?') !!}" method="GET" autocomplete="on">

                                <div class="input-group">

                                    <input name="search" type="text" class="form-control" placeholder="Eposta adresi ile ara" style="height: 37px !important;" value="{!! request()->has('search') ? request()->get('search') : null !!}">
                                    <span class="input-group-btn">

                                        @if(!int(request()->has('search')))
                                            <button class="btn btn-default" type="submit">
                                                <span class="fa fa-search"></span>
                                            </button>
                                        @endif
                                        @if(int(request()->has('search')))

                                            <button class="btn btn-default" type="button" onclick="window.location.href='{!! strtok(request()->fullUrl(),'?') !!}'">
                                                <span class="fa fa-times"></span>
                                            </button>
                                        @endif


                                    </span>

                                </div>

                            </form>
                        </div>
                    </div>

                    <hr style="margin-top:5px;margin-bottom: 15px;">

                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <td class="col-xs-1 text-center">#</td>
                            <td>Ad Soyad</td>
                            <td>Eposta adresi</td>
                            <td class="col-xs-1 text-center">Rol</td>
                            <td class="col-xs-1 text-center">Durumu</td>
                            <td>İşlemler</td>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr {!! !$user->activations()->first()->completed ? 'bg-gray' : null !!}>
                                <td class="text-center">{!! $user->id !!}</td>
                                <td>{!! $user->first_name !!} {!! $user->last_name !!}</td>
                                <td>{!! $user->email !!}</td>
                                <td class="text-center">{!! $user->roles()->first()->name !!}</td>
                                <td class="text-center">{!! $user->activations()->first()->completed ? 'Etkin' : 'Pasif' !!}</td>
                                <td class="col-xs-1 text-center">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown">
                                            <span class="fa fa-dot-circle-o"></span>
                                        </button>
                                        <ul class="dropdown-menu pull-right">
                                            <li> <a href="{!! route('back@user.all',['showUser' => $user->id]) !!}">Görüntüle</a> </li>
                                            <li> <a href="{!! route('back@user.all',['updateUser' => $user->id]) !!}">Düzenle</a> </li>
                                            <li role="separator" class="divider"></li>
                                            <li><a href="#">Mesaj Gönder</a></li>
                                            <li role="separator" class="divider"></li>
                                            <li> <a href="javascript:alert('Kullanılan sentinel paketinde Throttle modülü ban ve geçici ban özellikleri yoktur !')">Engelle</a> </li>
                                            <li> <a href="javascript:void(0)" onclick="a = confirm('{!! $user->first_name !!} {!! $user->last_name !!} kişiyi silmek istiyormusun ?'); if(a){ return window.location.href='{!! route('back@user.delete',['id' => $user->id]) !!}' } return false;">Kalıcı Olarak Sil</a> </li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    {!! $users->appends($appends)->links() !!}

                </div>

                @yield('showUser')
                @yield('updateUser')

            </div>

        </div>
    </div>

@stop
@section('scripts')
    <script>

        $(function(){
            const $role = $('select[name=role]');
            const $status = $('select[name=status]');
            const $uri = '{!! strtok(request()->fullUrl(),'?') !!}';

            if($role.length){
                $role.on('change',function(){
                    const $val = $(this).val();
                    if($status.length && !$status.val()){
                        window.location.href = $uri + '?role=' + $val;
                    }else if($status.length && $status.val()){
                        window.location.href = $uri + '?status=' + $status.val()  + '&role=' + $val;
                    }
                });
            }
            if($status.length){
                $status.on('change',function(){
                    const $val = $(this).val();
                    if($role.length && !$role.val()){
                        window.location.href = $uri + '?status=' + $val;
                    }else if($role.length && $role.val()){
                        window.location.href = $uri + '?role=' + $role.val() + '&status=' + $val;
                    }
                });
            }
        });
    </script>
    @stop