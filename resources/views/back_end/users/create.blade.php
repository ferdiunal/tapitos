@extends('Back::assets.main')
@section('main_content')
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12">

            <div class="panel panel-default">
                <div class="panel-body">
                    <form action="#">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="row">
                                    <legend class="col-xs-12">Kişisel Bilgileri</legend>

                                    <div class="row">

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <input type="text" placeholder="Adı" name="first_name" class="form-control">
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <input type="text" placeholder="Soyadı" name="last_name" class="form-control">
                                            </div>
                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                    </form>
                </div>
            </div>

            {{-- TODO Bu bölüm daha sonra doldurulacaktır. --}}
        </div>
    </div>
@stop