@section('role_update')
    <style>
        .ui-btn{
            width: auto !important;display: inline-block !important;
        }
        @media (min-width: 992px){
            .modal-lg {
                width: 1170px;
            }
        }

        .permissions{

            height:250px !important;
            max-height:250px !important;
            overflow-y: auto;
            overflow-x: hidden;

        }

        .permissions li + li {
            margin: 0 5px;
        }
        .permissions li {
            position: relative;
        }

        .permission-column li a{
            display: inline-block;
            position: absolute;
            right: -10px !important;
            top: -33px;
            z-index: 512;
        }

    </style>

    <div class="modal fade show in">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <a href="{!! route('back@user.role') !!}" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                    <h4 class="modal-title">Rol Adı : {!! $getRole->name !!}</h4>
                </div>

                <div class="modal-body">
                    <form action="{!! route('back@user.role.update') !!}" method="post" id="roleUpdate">
                        {!! csrf_field() !!}
                        <input type="hidden" name="roleId" value="{!! $getRole->id !!}">
                        <div class="form-group">
                            <label for="roleName">Rol Adı : </label>
                            <input type="text" id="roleName" value="{!! $getRole->name !!}" name="name" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="roleName">Kısa Adı : </label>
                            <input type="text" id="roleSlug" value="{!! $getRole->slug !!}" name="slug" class="form-control">
                        </div>

                        <legend>Yetkileri&emsp;<a style="font-size: 1rem;" href="javascript:void(0)" onclick="if(confirm('Bu işlemi gerçekleştirmek isityormusunuz ?')){ window.location.href='{!! route('back@user.role',['popup' => 'permissions','roleId' => $getRole->id]) !!}' }">Yeni Ekle</a></legend>

                        <div class="form-group clearfix">
                            <ul class="list-inline permissions">

                                @foreach($getRole->permissions as $name => $value)
                                    <li>
                                        <ul class="list-inline permission-column">
                                            <li><label><input type="checkbox" name="perm[{!! $name !!}]" {!! $value === true ? 'checked' : null !!}> {{ $name  }} </label></li>
                                            <li>
                                                <a class="btn btn-xs btn-default" href="{!! route('back@user.role',['do' => 'delete','roleId' => $getRole->id,'perm' => $name ]) !!}">
                                                    <span class="fa fa-times"></span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </form>
                </div>

                <div class="panel-footer text-center">
                    <div class="btn-group">
                        <a href="{!! route('back@user.role') !!}" class="btn btn-default">Geri</a>
                        <button class="btn btn-primary" form="roleUpdate">Güncelle</button>
                    </div>
                </div>

            </div>
        </div>
    </div>

@stop

@section('scripts')
{{--    <script src="{!! url('root/js/update.js') !!}"></script>--}}
    @stop
@section('styles')
    @stop