@section('role_permissions')
    <style>
        .ui-btn{
            width: auto !important;display: inline-block !important;
        }
        @media (min-width: 992px){
            .modal-lg {
                width: 1170px;
            }
        }

    </style>

    <div class="modal fade show in">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <a href="{!! route('back@user.role',['popup' => 'update','roleId' => $getRole->id]) !!}" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                    <h4 class="modal-title">Rol Adı : {!! $getRole->name !!}</h4>
                </div>

                <div class="modal-body">
                    <form action="{!! route('back@user.role') !!}" onsubmit="window.onbeforeunload=null;" id="permissionNew" role="form" method="post">
                        {!! csrf_field() !!}
                        <input type="hidden" name="popup" value="permissions">
                        <input type="hidden" name="roleId" value="{!! $getRole->id !!}">
                        <div class="form-group">
                            <label for="permission">Yetki Adı : </label>
                            <input type="text" tabindex="1" id="permission" name="permission_name" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="permission_status"> Aktif  </label>
                            <input type="checkbox" tabindex="2" id="permission_status" name="permission_status">
                        </div>

                    </form>
                </div>

                <div class="modal-footer">
                    <a class="btn btn-default" href="{!! route('back@user.role',['popup' => 'update','roleId' => $getRole->id]) !!}">Geri</a>
                    <button class="btn btn-primary" tabindex="3" form="permissionNew">EKLE</button>
                </div>

            </div>
        </div>
    </div>

@stop

@section('scripts')
    <script src="{!! url('root/js/update.js') !!}"></script>
    @stop
@section('styles')
    @stop