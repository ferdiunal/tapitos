@section('updateUser')
    <style>
        @media (min-width: 992px){
            .modal-lg {
                width: 1170px;
            }
        }

    </style>

    <div class="modal fade show in">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <a href="{!! route('back@user.all') !!}" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                    <h4 class="modal-title">{!! $user->first_name !!} {!! $user->last_name !!}</h4>
                </div>

                <div class="modal-body">
                    <form action="{!! route('back@user.update') !!}" id="userUpdateForm" onsubmit="return false;" autocomplete="off" method="post">
                        <input type="hidden" value="{!! $user->id !!}" name="user_id">
                        <div class="clearfix">

                            {{--<div class="col-sm-4 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <img src="{!! gravatar($user->email,'1000') !!}" class="img-thumbnail">
                                </div>
                            </div>--}}

                            <div class="col-sm-12 col-md-12 col-lg-12">

                                <div class="row">
                                    <div class="col-sm-6 col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <label for="first_name">Adı : </label>
                                            <input type="text" name="first_name" data-popover-pos="top" id="first_name" value="{!! $user->first_name !!}"
                                                   class="form-control">
                                        </div>
                                    </div>

                                    <div class="col-sm-6 col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <label for="last_name">Soyadı : </label>
                                            <input type="text" name="last_name" data-popover-pos="top" id="last_name" value="{!! $user->last_name !!}"
                                                   class="form-control">
                                        </div>
                                    </div>

                                    {{--<div class="col-sm-6 col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <label for="sex">Cinsiyeti : </label>
                                            <select name="sex" id="sex" class="form-control" title="Cinsiyet">
                                                <option class="hidden"></option>
                                                <option value="male">Erkek</option>
                                                <option value="female">Kadın</option>
                                            </select>
                                        </div>
                                    </div>--}}

                                </div>

                                <div class="row">
                                    <div class="col-sm-6 col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <label for="email">Eposta adresi : </label>
                                            <input type="text" id="email" name="email"  data-popover-pos="right" value="{!! $user->email !!}"
                                                   class="form-control">
                                        </div>
                                    </div>
                                    {{--<div class="col-sm-6 col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <label for="cell_phone">Cep Telefonu : </label>
                                            <input type="text" class="form-control" name="cell_phone" id="cell_phone">
                                        </div>
                                    </div>--}}
                                </div>
                                <div class="row">

                                    <div class="col-sm-6 col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <label for="role">Rolü : </label>
                                            <select name="role" id="role"  data-popover-pos="left" class="form-control" style="margin: 0;" title="Rol Seçin">
                                                <option class="hidden"></option>
                                                @if(request()->has('role'))
                                                    <option value="tumu">Tümü</option>
                                                @endif
                                                @foreach(\Sentinel::getRoleRepository()->createModel()->where('slug','!=','root')->get() as $roles)
                                                    <option value="{!! $roles->slug !!}" {!! $user->roles()->first()->slug === $roles->slug ? 'selected' : null !!}>{!! $roles->name !!}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                </div>

                                <div class="row">

                                    @if(!$user->activations()->first()->completed)
                                    <div class="col-sm-6 col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <button type="button" class="btn btn-default btn-block">Aktivasyon mail'i gönder</button>
                                        </div>
                                    </div>
                                    @endif


                                    <div class="col-sm-6 col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <button type="button" class="btn btn-default btn-block">Yeni Şife Gönder</button>
                                        </div>
                                    </div>

                                </div>

                            </div>

                            <div class="row">
                                <div class="col-sm-9 col-md-push-3">
                                    <a href="{!! route('back@user.all') !!}" class="btn btn-default">Vazgeç</a>
                                    <button class="btn btn-primary" type="submit"data-loading-text="Güncelleniyor..." onclick="root.user.update('#userUpdateForm')">Güncelle</button>
                                </div>
                            </div>

                        </div>

                    </form>

                </div>

            </div>
        </div>
    </div>

@stop

@section('scripts')
    <script src="{!! url('root/js/update.js') !!}"></script>
    @stop
@section('styles')
    @stop