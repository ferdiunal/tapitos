@extends('Back::assets.main')
@section('main_content')
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12">

            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-5">
                            <form action="{!! route('back@user.role.create') !!}" method="post">
                                {!! csrf_field() !!}
                                <legend>Yeni Rol Oluştur</legend>
                                <div class="form-group">
                                    <label for="name">Adı : </label>
                                    <input type="text" name="name" class="form-control" id="name">
                                </div>
                                <div class="form-group">
                                    <label for="name">Kısa adı : </label>
                                    <input type="text" name="slug" class="form-control" id="name">
                            </div> 
                                <button class="btn btn-default" type="submit">Oluştur</button>
                            </form>
                        </div>
                        <div class="col-sm-7">
                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>Adı</th>
                                    <th>Kısa Adı</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach(\Sentinel::getRoleRepository()->createModel()->where('id','!=',1)->get() as $roles)
                                    <tr>
                                        <td>{!! $roles->name !!}</td>
                                        <td>{!! $roles->slug !!}</td>
                                        <td class="col-xs-1 text-center">
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown">
                                                    <span class="fa fa-dot-circle-o"></span>
                                                </button>
                                                <ul class="dropdown-menu pull-right">
                                                    <li> <a href="{!! route('back@user.role',['popup' => 'update','roleId' => $roles->id]) !!}">Düzenle</a> </li>
                                                    <li> <a href="{!! route('back@user.role',['do' => 'forceDelete','roleId' => $roles->id]) !!}">Kalıcı olarak sil</a> </li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
            @yield('role_update')
            @yield('role_permissions')
        </div>
    </div>
@stop