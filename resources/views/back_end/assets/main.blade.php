@extends('Back::layout')
@section('main')
        <!-- BEGIN WRAPPER -->
<div id="wrapper">
    @yield('sidebar')
            <!-- BEGIN MAIN CONTENT -->
    <div id="main-content" class="dashboard">
        @yield('main_content')
    </div>
    <!-- END MAIN CONTENT -->

</div>
<!-- END WRAPPER -->
@stop