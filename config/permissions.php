<?php
/**
 * Created by PhpStorm.
 * User: ferdi
 * Date: 08.04.2016
 * Time: 13:41
 */

return [

    // Permissions
    [
        'title' =>  'Üye oluşturur',
        'perm'  =>  'user.created',
    ],
    [
        'title' =>  'Üye Günceller',
        'perm'  =>  'user.updated',
    ],
    [
        'title' =>  'Üye Siler',
        'perm'  =>  'user.deleted',
    ],
    [
        'title' =>  'Üye Görüntüler',
        'perm'  =>  'user.view',
    ],
    [
        'title' =>  'Kategori Oluşturur',
        'perm'  =>  'category.created',
    ],
    [
        'title' =>  'Kategori Günceller',
        'perm'  =>  'category.updated',
    ],
    [
        'title' =>  'Kategori Siler',
        'perm'  =>  'category.updated',
    ],
    [
        'title' =>  'Kategori Görüntüler',
        'perm'  =>  'category.view',
    ],
    [
        'title' =>  'Ürün Oluşturur',
        'perm'  =>  'product.created',
    ],
    [
        'title' =>  'Ürün Günceller',
        'perm'  =>  'product.updated',
    ],
    [
        'title' =>  'Ürün Siler',
        'perm'  =>  'product.deleted',
    ],
    [
        'title' =>  'Ürün Siler',
        'perm'  =>  'product.deleted',
    ],
    [
        'title' =>  'Ürün Görüntüler',
        'perm'  =>  'product.deleted',
    ],
    [
        'title' =>  'Ürüne Yorum Yapar',
        'perm'  =>  'product.commend.created',
    ],
    [
        'title' =>  'Ürüne Yapılan Yorumu Günceller',
        'perm'  =>  'product.commend.updated',
    ],
    [
        'title' =>  'Ürüne Yapılan Yorumu Siler',
        'perm'  =>  'product.commend.deleted',
    ],
    [
        'title' =>  'Ürüne Yapılan Yorumu Görür',
        'perm'  =>  'product.commend.view',
    ],
      
];